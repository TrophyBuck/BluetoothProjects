package buck.christopher.bluetoothprojects;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by Chris on 4/19/2016.
 */
public class ClientTask extends AsyncTask<BluetoothDevice, Void, Boolean>
{
    private MainActivity activity = null;

    void detach() {
        activity = null;
    }

    public ClientTask(MainActivity activity)
    {
        attach(activity);
    }

    void attach(MainActivity activity) {
        if (activity == null)
            throw new IllegalArgumentException("Activity cannot be null");

        this.activity = activity;
    }

    @Override
    public Boolean doInBackground(BluetoothDevice... params)
    {
        if(params[0]== null)
            return false;

        try
        {
            BluetoothSocket connection = params[0].createRfcommSocketToServiceRecord(UUID.fromString(Constants.UUID_STRING));
            connection.connect();

            InputStream inputStream = connection.getInputStream();
            OutputStream outputStream = connection.getOutputStream();

            String message = Constants.TESTING_MESSAGE;
            outputStream.write(message.getBytes());     //Message can be anything that can be converted to a byte array- strings, sound, etc.  We use a string here
            outputStream.flush();
            Thread.sleep(1000);
            connection.close();
            return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        if(result)
            Constants.shortToast(activity.getApplicationContext(), "Success");
        else
            Constants.shortToast(activity.getApplicationContext(), "Failure");
    }
}
