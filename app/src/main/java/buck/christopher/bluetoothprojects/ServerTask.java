package buck.christopher.bluetoothprojects;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

/**
 * Created by Chris on 4/19/2016.
 */
public class ServerTask extends AsyncTask<BluetoothAdapter, byte[], Void>
{
    private MainActivity activity = null;
    private StringBuilder stringBuilder;

    void detach() {
        activity = null;
    }

    public ServerTask(MainActivity activity)
    {
        attach(activity);
        stringBuilder = new StringBuilder();
    }

    void attach(MainActivity activity) {
        if (activity == null)
            throw new IllegalArgumentException("Activity cannot be null");

        this.activity = activity;
    }

    @Override
    public Void doInBackground(BluetoothAdapter... params)
    {
        if(params[0]== null)
            return null;

        try
        {
            BluetoothServerSocket uuidServer = params[0].listenUsingRfcommWithServiceRecord("Test Server", UUID.fromString(Constants.UUID_STRING));
            BluetoothSocket connection = uuidServer.accept();
            uuidServer.close();

            InputStream inputStream = connection.getInputStream();
            OutputStream outputStream = connection.getOutputStream();

            byte[] buffer = new byte[1024];
            int numberOfBytesRead = 0;

            while(true) //Run until an exception occurs
            {
                numberOfBytesRead = inputStream.read(buffer);
                if(numberOfBytesRead != -1)
                {
                    publishProgress(buffer);
                }
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    protected void onProgressUpdate(byte[]... buffer)
    {
        String text = null;
        try
        {
            text = new String(buffer[0], "UTF-8");
            stringBuilder.append(text);

            if(stringBuilder.toString().contains("\n")) //if we have at least one complete message
            {
                String[] messages = stringBuilder.toString().split("\n");   //split the string into all parts
                stringBuilder = new StringBuilder();    //Reset the string builder
                stringBuilder.append(messages[messages.length-1]);  //append the last part (which is not a complete message) to the stringbuilder so it can be finished later

                for(int i = 0; i < messages.length - 1; i++)    //loop through all complete messages and display them
                {
                    Constants.shortToast(activity.getApplicationContext(), messages[i]);
                }
            }
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(Void v)
    {
        Constants.longToast(activity.getApplicationContext(), "Closed Connection");
    }
}
