package buck.christopher.bluetoothprojects;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity{

    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<BluetoothDevice> availableDevices;
    private Button bluetoothButton;
    private boolean launchClient = false;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //Constants.shortToast(getApplicationContext(), "Found: " + device.getName());
                availableDevices.add(device);
                Log.d(Constants.DEBUG_TAG, device.getAddress());
                if(launchClient && device.getAddress().equals(Constants.TARGET_MAC))
                {
                    launchClient = false;
                    Log.d(Constants.DEBUG_TAG, "Found server");
                    Constants.shortToast(MainActivity.this.getApplicationContext(), "Found Server");
                    ClientTask clientThread = new ClientTask(MainActivity.this);
                    clientThread.execute(device);
                }
            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action))
            {
                Constants.shortToast(context, "Started Discovery");
                availableDevices = new ArrayList<>();
            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
            {
                Constants.shortToast(context, "Finished Discovery");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothButton = (Button)findViewById(R.id.button1);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) // Device does not support Bluetooth
            Constants.longToast(getApplicationContext(), "Bluetooth not supported on this device, app is useless.");

        else if(mBluetoothAdapter.isEnabled())  //If bluetooth was already enabled, change button text
            bluetoothButton.setText(getString(R.string.bluetooth_disable_text));

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);
    }

    public void enableBluetooth(View view)
    {
        if(mBluetoothAdapter != null)
        {
            if(!mBluetoothAdapter.isEnabled())
            {
                //request Bluetooth access
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
            }
            else
            {
                Constants.shortToast(getApplicationContext(), "Bluetooth disabled");
                mBluetoothAdapter.disable();
                bluetoothButton.setText(getString(R.string.bluetooth_enable_text));
            }
        }
    }

    public void discoverDevices(View view)
    {
        if(mBluetoothAdapter != null)
        {
            if(!bluetoothEnabled())
                return;

            //Android 6 makes it so Bluetooth requires COARSE or FINE location permission.  You have to explicitly ask for those here
            List<String> ungrantedPermissions = Constants.getUngrantedExplicitPermissions(MainActivity.this, Constants.BLUETOOTH_DISCOVERY_LOCATION_PERMISSION);
            if(ungrantedPermissions != null && Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1)
            {
                requestPermissions(ungrantedPermissions.toArray(new String[0]), Constants.PERMISSIONS_REQUEST_BLUETOOTH_DISCOVERY);
            }
            else
            {
                discover();
            }
        }
    }

    public boolean discover()
    {
        availableDevices = new ArrayList<BluetoothDevice>();
        if(mBluetoothAdapter.startDiscovery())
        {
            //Constants.shortToast(getApplicationContext(), "Discovery Started");
            return true;
        }
        else
        {
            Constants.shortToast(getApplicationContext(), "Failed to start discovery");
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        switch(requestCode){

            case Constants.PERMISSIONS_REQUEST_BLUETOOTH_DISCOVERY:

                if(Constants.getUngrantedExplicitPermissions(MainActivity.this, Constants.BLUETOOTH_DISCOVERY_LOCATION_PERMISSION) == null)
                {
                    discover();
                }
                else
                {
                    Constants.longToast(getApplicationContext(), "Cannot run Bluetooth Discovery without Location permission in Android 6");
                }
        }
    }

    public void enableDiscovery(View view)
    {
        if(mBluetoothAdapter != null)
        {
            if(!bluetoothEnabled())
                return;

            Intent discoverDevices = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(discoverDevices, Constants.REQUEST_DISCOVER_BT);
        }
    }

    public void viewPairedDevices(View view)
    {
        if(mBluetoothAdapter != null)
        {
            if(!bluetoothEnabled())
                return;

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();  //Get all devices that are paired with this
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("List of Paired Devices");
            final List<BluetoothDevice> deviceList = new ArrayList<BluetoothDevice>();
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.select_dialog_multichoice);
            for(BluetoothDevice a : pairedDevices)
            {
                deviceList.add(a);
                adapter.add(a.getName());
            }
            builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {    //This is where you would do something with the device that was selected
                    Constants.shortToast(getApplicationContext(), "Selected: " + deviceList.get(which).toString());
                }
            });
            builder.create().show();
        }
    }

    public void clientConnect(View view)
    {
        if(mBluetoothAdapter != null && bluetoothEnabled())
        {
            if(Constants.getUngrantedExplicitPermissions(MainActivity.this, Constants.BLUETOOTH_DISCOVERY_LOCATION_PERMISSION) == null)
            {
                launchClient = true;
                discover();
            }
            else
            {
                Constants.shortToast(getApplicationContext(), "Run discovery method to enable required permissions");
            }
        }
    }

    public void serverBroadcast(View view)
    {
        if(mBluetoothAdapter != null && bluetoothEnabled())
        {
            Intent discoverDevices = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(discoverDevices, Constants.REQUEST_DISCOVER_BT_FOR_SERVER_THREAD);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode)
        {
            case(Constants.REQUEST_ENABLE_BT):
                if(resultCode == Activity.RESULT_OK)
                {
                    //Bluetooth should be enabled, make connection
                    Constants.shortToast(getApplicationContext(), "Bluetooth enabled");
                    bluetoothButton.setText(getString(R.string.bluetooth_disable_text));
                }
                else if(resultCode == Activity.RESULT_CANCELED)
                {
                    //User responded error or there was an error
                    Constants.shortToast(getApplicationContext(), "Bluetooth was not enabled");
                }
                break;
            case(Constants.REQUEST_DISCOVER_BT):
                if(resultCode == Activity.RESULT_CANCELED)
                {
                    Constants.shortToast(getApplicationContext(), "Phone not made discoverable");
                }
                else    //otherwise result code will be equal to the duration that the device is discoverable
                {
                    Constants.longToast(getApplicationContext(), "Phone discoverable for " + resultCode + " seconds");
                }
                break;
            case(Constants.REQUEST_DISCOVER_BT_FOR_SERVER_THREAD):
                if(resultCode == Activity.RESULT_CANCELED)
                {
                    Constants.shortToast(getApplicationContext(), "Broadcast not started");
                }
                else
                {
                    Constants.shortToast(getApplicationContext(), "Started broadcast");
                    ServerTask serverTask = new ServerTask(MainActivity.this);
                    serverTask.execute(mBluetoothAdapter);
                }
            default:
                break;
        }
    }

    public boolean bluetoothEnabled()
    {
        if(!mBluetoothAdapter.isEnabled())
        {
            Constants.shortToast(getApplicationContext(), "Bluetooth not enabled, enable Bluetooth first");
        }
        return mBluetoothAdapter.isEnabled();
    }

    @Override
    public void onDestroy()
    {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }
}