package buck.christopher.bluetoothprojects;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chris on 4/12/2016.
 */
public class Constants
{
    public static final int REQUEST_ENABLE_BT = 1;
    public static final int REQUEST_DISCOVER_BT = 2;
    public static final int REQUEST_DISCOVER_BT_FOR_SERVER_THREAD = 3;
    public static final String DEBUG_TAG = "DEBUG";
    public static final String[] BLUETOOTH_DISCOVERY_LOCATION_PERMISSION = {"android.permission.ACCESS_COARSE_LOCATION"};
    public static final int PERMISSIONS_REQUEST_BLUETOOTH_DISCOVERY = 200;
    public static final String UUID_STRING = "38400000-8cf0-11bd-b23e-10b96e4ef00d";

    //TODO: I have hard coded the address and message here, but in a practical application you would probably have to select the device from a list
    public static final String TARGET_MAC = "10:68:3F:DC:B6:E4";
    public static final String TESTING_MESSAGE = "Testing message\nThis is the second part\nNow we are finished\n";

    public static void shortToast(Context context, String text)
    {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void longToast(Context context, String text)
    {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    /**
     * Returns a list of all ungranted permissions required in Android 6, or null if either all permissions are granted OR version is not Android 6 or higher
     * @param activity
     * @return List of ungranted permissions, or null if all necessary permissions have been granted
     */
    public static List<String> getUngrantedExplicitPermissions(MainActivity activity, String[] permissionsToRequest)
    {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1)
        {
            boolean allPermissionsGranted = true;
            List<String> ungrantedPermissions = new ArrayList<String>();
            for(String perm : permissionsToRequest)
            {
                if(activity.checkSelfPermission(perm) != PackageManager.PERMISSION_GRANTED)
                {
                    allPermissionsGranted = false;
                    ungrantedPermissions.add(perm);
                }
            }
            if(!allPermissionsGranted)
            {
                return ungrantedPermissions;
            }

            return null;
        }
        return null;
    }

    private Constants(){    throw new AssertionError(); }
}
